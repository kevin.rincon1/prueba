package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.ObjectRequestDto;
import com.pragma.powerup.application.dto.request.ObjectUpdateRequestDto;
import com.pragma.powerup.application.dto.response.ObjectResponseDto;
import com.pragma.powerup.application.mapper.IObjectRequestMapper;
import com.pragma.powerup.application.mapper.IObjectResponseMapper;
import com.pragma.powerup.domain.api.IObjectServicePort;
import com.pragma.powerup.domain.model.ObjectModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ObjectHandlerTest {
    @InjectMocks
    ObjectHandler objectHandler;
    @Mock
    IObjectServicePort objectServicePort;
    @Mock
    IObjectRequestMapper objectRequestMapper;
    @Mock
    IObjectResponseMapper objectResponseMapper;
    ObjectRequestDto objectRequestDto;
    ObjectUpdateRequestDto objectUpdateRequestDto;
    ObjectResponseDto objectResponseDto;
    ObjectModel objectModel;
    private static final Long id = Long.valueOf(1);
    private static final String name = "Pepe";


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        objectRequestDto = Mockito.mock(ObjectRequestDto.class);
        when(objectRequestDto.getName()).thenReturn(name);

        objectResponseDto = Mockito.mock(ObjectResponseDto.class);
        when(objectResponseDto.getId()).thenReturn(id);
        when(objectResponseDto.getName()).thenReturn(name);

        objectUpdateRequestDto = Mockito.mock(ObjectUpdateRequestDto.class);
        when(objectUpdateRequestDto.getId()).thenReturn(id);
        when(objectUpdateRequestDto.getName()).thenReturn(name);

        objectModel = Mockito.mock(ObjectModel.class);
        when(objectModel.getId()).thenReturn(id);
        when(objectModel.getName()).thenReturn(name);

        when(objectRequestMapper.toObject(objectRequestDto)).thenReturn(objectModel);
        when(objectResponseMapper.toResponseList(Arrays.asList(objectModel))).thenReturn(Arrays.asList(objectResponseDto));
        when(objectServicePort.getAllObjects()).thenReturn(Arrays.asList(objectModel));
    }
    @Test
    void saveObject() {
        objectHandler.saveObject(objectRequestDto);
    }

    @Test
    void getAllObjects() {
        List<ObjectResponseDto> list = objectHandler.getAllObjects();
        assertEquals(list, Arrays.asList(objectResponseDto));
    }

    @Test
    void updateObject() {
        objectHandler.updateObject(objectUpdateRequestDto);
    }

    @Test
    void deleteObject() {
        objectHandler.deleteObject(id);
    }
}