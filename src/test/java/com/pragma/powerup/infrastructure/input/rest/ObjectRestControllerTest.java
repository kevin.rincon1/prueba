package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.ObjectRequestDto;
import com.pragma.powerup.application.dto.request.ObjectUpdateRequestDto;
import com.pragma.powerup.application.dto.response.ObjectResponseDto;
import com.pragma.powerup.application.handler.impl.ObjectHandler;
import com.pragma.powerup.domain.model.ObjectModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ObjectRestControllerTest {
    @InjectMocks
    ObjectRestController objectRestController;
    @Mock
    ObjectHandler objectHandler;
    ObjectRequestDto objectRequestDto;
    ObjectUpdateRequestDto objectUpdateRequestDto;
    ObjectResponseDto objectResponseDto;
    ObjectModel objectModel;
    private static final Long id = Long.valueOf(1);
    private static final String name = "Pepe";


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        objectRequestDto = Mockito.mock(ObjectRequestDto.class);
        when(objectRequestDto.getName()).thenReturn(name);

        objectUpdateRequestDto = Mockito.mock(ObjectUpdateRequestDto.class);
        when(objectUpdateRequestDto.getId()).thenReturn(id);
        when(objectUpdateRequestDto.getName()).thenReturn(name);

        objectResponseDto = Mockito.mock(ObjectResponseDto.class);
        when(objectResponseDto.getId()).thenReturn(id);
        when(objectResponseDto.getName()).thenReturn(name);

        objectModel = Mockito.mock(ObjectModel.class);
        when(objectModel.getId()).thenReturn(id);
        when(objectModel.getName()).thenReturn(name);

        when(objectHandler.getAllObjects()).thenReturn(Arrays.asList(objectResponseDto));
    }
    @Test
    void saveObject() {
        objectRestController.saveObject(objectRequestDto);
    }

    @Test
    void getAllObjects() {
        ResponseEntity<List<ObjectResponseDto>> list = objectRestController.getAllObjects();
        assertEquals(list, ResponseEntity.ok(Arrays.asList(objectResponseDto)));
    }

    @Test
    void update() {
        objectRestController.update(objectUpdateRequestDto);
    }

    @Test
    void delete() {
        objectRestController.delete(id);
    }
}