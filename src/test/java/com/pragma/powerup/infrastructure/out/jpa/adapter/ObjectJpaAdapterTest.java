package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.ObjectModel;
import com.pragma.powerup.domain.spi.IObjectPersistencePort;
import com.pragma.powerup.domain.usecase.ObjectUseCase;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.ObjectEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IObjectEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IObjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class ObjectJpaAdapterTest {
    @InjectMocks
    ObjectJpaAdapter objectJpaAdapter;
    @Mock
    IObjectRepository objectRepository;
    @Mock
    IObjectEntityMapper objectEntityMapper;
    private ObjectModel objectModel;
    private ObjectEntity objectEntity;
    private static final Long id = Long.valueOf(1);
    private static final String name = "Pepe";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        objectModel = Mockito.mock(ObjectModel.class);
        when(objectModel.getId()).thenReturn(id);
        when(objectModel.getName()).thenReturn(name);
        objectEntity = Mockito.mock(ObjectEntity.class);
        when(objectEntity.getId()).thenReturn(id);
        when(objectEntity.getName()).thenReturn(name);

        when(objectRepository.save(objectEntity)).thenReturn(objectEntity);
        when(objectEntityMapper.toEntity(objectModel)).thenReturn(objectEntity);
        when(objectEntityMapper.toObjectModel(objectEntity)).thenReturn(objectModel);
        when(objectEntityMapper.toObjectModelList(Arrays.asList(objectEntity))).thenReturn(Arrays.asList(objectModel));

        when(objectRepository.findAll()).thenReturn(Arrays.asList(objectEntity));

        when(objectRepository.getById(id)).thenReturn(objectEntity);
    }

    @Test
    void saveObject() {
        ObjectModel Object =  objectJpaAdapter.saveObject(objectModel);
        assertEquals(Object, objectModel);
    }

    @Test
    void getAllObjects() {
        List<ObjectModel> list = objectJpaAdapter.getAllObjects();
        assertEquals(list, Arrays.asList(objectModel));
    }
    @Test
    void getAllObjectsEmpty() {
        when(objectRepository.findAll()).thenReturn(Arrays.asList());
        assertThrows(NoDataFoundException.class, () -> {
            objectJpaAdapter.getAllObjects();
        });
    }

    @Test
    void updateNullObject() {
        when(objectRepository.getById(id)).thenReturn(null);
        assertThrows(NullPointerException.class, () -> {
            objectJpaAdapter.updateObject(objectModel);
        });
    }
    @Test
    void updateObject() {
        ObjectModel Object =  objectJpaAdapter.updateObject(objectModel);
        assertEquals(Object, objectModel);
    }

    @Test
    void deleteObject() {
        objectJpaAdapter.deleteObject(id);
    }
}