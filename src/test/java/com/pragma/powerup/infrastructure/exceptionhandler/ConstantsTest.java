package com.pragma.powerup.infrastructure.exceptionhandler;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.*;

class ConstantsTest {
    @InjectMocks
    Constants constants;
    public String USER_CREATED_MESSAGE = "User created successfully";
    public String USER_UPDATE_MESSAGE = "User update successfully";
    public String USER_DELETE_MESSAGE = "User delete successfully";
    public String MENSAJE = "Menssaje: ";
    @Test
    public void TestMensage(){
        assertEquals(constants.USER_CREATED_MESSAGE, USER_CREATED_MESSAGE);
        assertEquals(constants.USER_UPDATE_MESSAGE, USER_UPDATE_MESSAGE);
        assertEquals(constants.USER_DELETE_MESSAGE, USER_DELETE_MESSAGE);
        assertEquals(constants.MENSAJE, MENSAJE);
    }

}