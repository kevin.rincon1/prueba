package com.pragma.powerup.infrastructure.configuration;

import com.pragma.powerup.infrastructure.out.jpa.mapper.IObjectEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IObjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class BeanConfigurationTest {
    @InjectMocks
    BeanConfiguration beanConfiguration;
    @Mock
    IObjectRepository objectRepository;
    @Mock
    IObjectEntityMapper objectEntityMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

    }
    @Test
    void objectPersistencePort() {
        beanConfiguration.objectPersistencePort();
    }

    @Test
    void objectServicePort() {
        beanConfiguration.objectServicePort();
    }
}