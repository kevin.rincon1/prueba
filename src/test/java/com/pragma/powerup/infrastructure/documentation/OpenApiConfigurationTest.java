package com.pragma.powerup.infrastructure.documentation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class OpenApiConfigurationTest {
    @InjectMocks
    OpenApiConfiguration openApiConfiguration;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void customOpenApi() {
        openApiConfiguration.customOpenApi("${appdescription}", "${appversion}");
    }
}