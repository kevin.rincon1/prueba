package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.model.ObjectModel;
import com.pragma.powerup.domain.spi.IObjectPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ObjectUseCaseTest {
    @InjectMocks
    ObjectUseCase objectUseCase;
    @Mock
    IObjectPersistencePort objectPersistencePort;
    private ObjectModel objectModel;
    private static final Long id = Long.valueOf(1);
    private static final String name = "Pepe";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        objectModel = Mockito.mock(ObjectModel.class);

        when(objectModel.getId()).thenReturn(id);
        when(objectModel.getName()).thenReturn(name);
    }

    @Test
    void saveObject() {
        objectUseCase.saveObject(objectModel);
    }

    @Test
    void getAllObjects() {
        when(objectUseCase.getAllObjects()).thenReturn(Arrays.asList(objectModel));
        assertNotNull(objectUseCase.getAllObjects());

        when(objectPersistencePort.getAllObjects()).thenReturn(Arrays.asList(objectModel));
        assertEquals(objectPersistencePort.getAllObjects(), Arrays.asList(objectModel));
    }

    @Test
    void updateObject() {
        objectUseCase.updateObject(objectModel);
    }

    @Test
    void deleteObject() {
        objectUseCase.deleteObject(id);
    }
}