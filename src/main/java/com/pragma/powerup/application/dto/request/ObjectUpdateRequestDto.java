package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjectUpdateRequestDto {
    private Long id;
    private String name;
}
