package com.pragma.powerup.domain.api;

import com.pragma.powerup.application.dto.request.ObjectRequestDto;
import com.pragma.powerup.domain.model.ObjectModel;

import java.util.List;

public interface IObjectServicePort {

    void saveObject(ObjectModel objectModel);
    void updateObject(ObjectModel objectModel);
    void deleteObject(Long id);
    List<ObjectModel> getAllObjects();
}