package com.pragma.powerup.domain.spi;

import com.pragma.powerup.application.dto.request.ObjectRequestDto;
import com.pragma.powerup.domain.model.ObjectModel;
import java.util.List;

public interface IObjectPersistencePort {
    ObjectModel saveObject(ObjectModel objectModel);
    ObjectModel updateObject(ObjectModel objectModel);
    void deleteObject(Long id);
    List<ObjectModel> getAllObjects();
}