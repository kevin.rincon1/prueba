FROM gradle:jdk17 as gradleimage
COPY . /home/gradle/source
WORKDIR /home/gradle/source
RUN ./gradlew clean build

FROM openjdk:17
EXPOSE 8081
COPY --from=gradleimage /home/gradle/source/build/libs/power-up-arquetipo-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app
ENTRYPOINT ["java", "-jar", "power-up-arquetipo-0.0.1-SNAPSHOT.jar"]